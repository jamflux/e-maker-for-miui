setlocal DisableDelayedExpansion
 
set BUILDIR=ramdisk
set INTEXTFILE=fstab.mt6797
 
set OUTTEXTFILE=temp.txt
set SEARCHTEXT=forceencrypt
set VER=encryptable
set OUTPUTLINE=
 
for /f "tokens=1,* delims=¶" %%A in ( '"type %BUILDIR%\%INTEXTFILE%"') do (
    SET string=%%A
    setLocal EnableDelayedExpansion
    SET modified=!string:%SEARCHTEXT%=%VER%!
    echo.!modified! >> %BUILDIR%\%OUTTEXTFILE%
    endlocal
 )
 
del %BUILDIR%\%INTEXTFILE%
rename %BUILDIR%\%OUTTEXTFILE% %INTEXTFILE%