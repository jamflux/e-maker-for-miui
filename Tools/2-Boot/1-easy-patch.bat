@echo OFF
SETLOCAL ENABLEDELAYEDEXPANSION


:start
if not exist ramdisk (
echo  *******************************
echo  There is not ramdisk folder
echo  to process. First unpack boot.img
echo  Cant't continue.
echo  *******************************
pause>nul
exit
)
						
:home
if exist ramdisk\fstab.mt6797 (
call android_win_tools\1-dm-mt6797.bat
call android_win_tools\2-encryp-mt6797.bat
call repackimg.bat
echo  *******************************
echo  Old boot.img was replaced by
echo  Patched one. Enjoy!
echo  *******************************
pause>nul
exit
)
call android_win_tools\1-dm.bat
call android_win_tools\2-encryp.bat
call repackimg.bat
echo  *******************************
echo  Old boot.img was replaced by
echo  Patched one. Enjoy!
echo  *******************************
pause>nul
exit

