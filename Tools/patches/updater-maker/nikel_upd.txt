mount("ext4", "EMMC", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/cust", "/cust", "max_batch_time=0,commit=1,data=ordered,barrier=1,errors=panic,nodelalloc");
format("ext4", "EMMC", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/system", "0", "/system");
delete("/cust/app/customized/ota-miui-MiuiGlobalForum/ota-miui-MiuiGlobalForum.apk",
       "/cust/app/customized/ota-miui-BBS_MSITE/ota-miui-BBS_MSITE.apk",
       "/cust/app/customized/ota-partner-GooglePinyin/ota-partner-GooglePinyin.apk",
       "/cust/app/customized/ota-miui-MiGalleryLockscreen_global/ota-miui-MiGalleryLockscreen_global.apk",
       "/cust/app/customized/ota-partner-GoogleZhuyin/ota-partner-GoogleZhuyin.apk",
       "/cust/app/customized/partner-XMRemoteController/partner-XMRemoteController.apk",
       "/cust/app/customized/ota-miui-GlobalMiShop/ota-miui-GlobalMiShop.apk",
       "/cust/app/customized/ota-miui-WaliLiveIndia/ota-miui-WaliLiveIndia.apk");
delete("/cust/app/customized/recommended-3rd-com.immomo.momo.apk",
       "/cust/app/customized/recommended-3rd-com.immomo.momo/recommended-3rd-com.immomo.momo.apk",
       "/cust/app/customized/recommended-3rd-com.juanpi.ui.apk",
       "/cust/app/customized/recommended-3rd-com.juanpi.ui/recommended-3rd-com.juanpi.ui.apk",
       "/cust/app/customized/recommended-3rd-com.qiyi.video.apk",
       "/cust/app/customized/recommended-3rd-com.qiyi.video/recommended-3rd-com.qiyi.video.apk",
       "/cust/app/customized/recommended-3rd-com.qzone.apk",
       "/cust/app/customized/recommended-3rd-com.qzone/recommended-3rd-com.qzone.apk",
       "/cust/app/customized/recommended-3rd-com.sina.weibo.apk",
       "/cust/app/customized/recommended-3rd-com.sina.weibo/recommended-3rd-com.sina.weibo.apk",
       "/cust/app/customized/recommended-3rd-com.tencent.qqmusic.apk",
       "/cust/app/customized/recommended-3rd-com.tencent.qqmusic/recommended-3rd-com.tencent.qqmusic.apk",
       "/cust/app/customized/recommended-3rd-com.tuniu.app.ui.apk",
       "/cust/app/customized/recommended-3rd-com.tuniu.app.ui/recommended-3rd-com.tuniu.app.ui.apk",
       "/cust/app/customized/recommended-3rd-tv.danmaku.bili.apk",
       "/cust/app/customized/recommended-3rd-tv.danmaku.bili/recommended-3rd-tv.danmaku.bili.apk",
       "/cust/app/customized/partner-XunfeiSpeechService3.apk",
       "/cust/app/customized/partner-XunfeiSpeechService3/partner-XunfeiSpeechService3.apk");
package_extract_dir("cust", "/cust") || abort("Failed to extract dir from \"cust\" to \"/cust\".");
set_metadata_recursive("/cust", "uid", 0, "gid", 0, "dmode", 0755, "fmode", 0644, "capabilities", 0x0, "selabel", "u:object_r:system_file:s0");
show_progress(0.200000, 10);
package_extract_file("boot.img", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/boot");
assert(write_preloader("preloader.img"));

# ---- radio update tasks ----

ui_print("Patching firmware images...");
package_extract_file("firmware-update/logo.bin", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/logo");
package_extract_file("firmware-update/md1arm7.img", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/md1arm7");
package_extract_file("firmware-update/trustzone.bin", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/tee1");
package_extract_file("firmware-update/lk.bin", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/lk");
package_extract_file("firmware-update/md1rom.img", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/md1img");
package_extract_file("firmware-update/tinysys-scp.bin", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/scp1");
package_extract_file("firmware-update/md1dsp.img", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/md1dsp");
package_extract_file("firmware-update/md3rom.img", "/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/md3img");
package_extract_file("META-INF/com/miui/miui_update", "/cache/miui_update");
set_metadata("/cache/miui_update", "uid", 0, "gid", 0, "mode", 0555, "capabilities", 0x0);
run_program("/cache/miui_update");
delete("/cache/miui_update");
show_progress(0.600000, 85);
ui_print("Patching system image unconditionally...");
block_image_update("/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/system", package_extract_file("system.transfer.list"), "system.new.dat", "system.patch.dat");
show_progress(0.100000, 2);
unmount("/cust");


